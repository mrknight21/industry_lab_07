package ictgradschool.industry.lab07.ex06;

import ictgradschool.Keyboard;

import java.security.Key;

/**
 * TODO Write a program according to the Exercise Six guidelines in your lab handout.
 */
public class ExerciseSix {

    public void start() {
        String sequence = getSequence();
        String outcome = "";
        if (Character.isLetter(sequence.charAt(0)))
        {outcome += sequence.charAt(0);}
        for (int i = 0; i < sequence.length(); i++)
        {if (sequence.charAt(i) == ' ' && Character.isLetter(sequence.charAt(i+1)))
         {
             outcome = outcome + ' '+sequence.charAt(i+1);
         }
        }
        System.out.print("You entered: "+outcome.toUpperCase());
    }

    // TODO Write some methods to help you.
    public class ExceedMaxStringLengthException extends Exception {
        public ExceedMaxStringLengthException(String message) { super(message); }
    }
    public class InvalidWordException extends Exception {

        public InvalidWordException(String message) { super(message); }
    }

    public String getSequence()

    {
        String sequence = "";
        int indicator = 0;
        do{
            System.out.println("Please enter your String sequence of at most 100 words: ");
            try{
                sequence = Keyboard.readInput();
                if (sequence.length()>100 || sequence.length()<1)
                {
                    throw new ExceedMaxStringLengthException("The sequence does not have right number of characters(1-100)");
                }
                else {
                    if (validationString(sequence))
                    {
                        indicator++;
                    };
                }

            }
            catch (ExceedMaxStringLengthException e){
                System.out.println(e.getMessage());
            }
            catch (InvalidWordException e){System.out.println(e.getMessage());}


        }while(indicator == 0);
        return sequence;
    }

   public boolean validationString (String sequence) throws InvalidWordException
   {
       if (!Character.isLetter(sequence.charAt(0)) && !((sequence.charAt(0))==' '))
       {
           throw new InvalidWordException("Some words in the sequence are not valid1.");
       }
       else{
           for (int i = 0; i < sequence.length(); i++)
           {
               if ( (sequence.charAt(i)== ' ') && !(Character.isLetter(sequence.charAt(i+1))))
                   {
                       throw new InvalidWordException("Some words in the sequence are not valid2.");
                   }
               }
           }
       return true;
   }




    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseSix().start();
    }
}
