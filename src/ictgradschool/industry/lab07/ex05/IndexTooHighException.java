package ictgradschool.industry.lab07.ex05;

/**
 * Created by mche618 on 3/04/2017.
 */
public class IndexTooHighException extends  Exception{
    public IndexTooHighException (String message){
        super(message);
    }

}
