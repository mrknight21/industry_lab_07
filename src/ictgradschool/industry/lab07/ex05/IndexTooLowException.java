package ictgradschool.industry.lab07.ex05;

/**
 * Created by mche618 on 3/04/2017.
 */
public class IndexTooLowException extends Exception {
    public IndexTooLowException (String message){
        super(message);
    }
}
