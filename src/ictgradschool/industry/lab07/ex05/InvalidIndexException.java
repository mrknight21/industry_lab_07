package ictgradschool.industry.lab07.ex05;

/**
 * Created by mche618 on 3/04/2017.
 */
public class InvalidIndexException extends Exception{
    public InvalidIndexException (String message){
        super(message);
    }
}
